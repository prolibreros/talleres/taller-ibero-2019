Tuñón Oviedo, Alberto Hidalgo. “Materialismo Filosófico.” Eikasia. Revista de filosofía, no. 2 (Enero 2006): 2. http://www.revistadefilosofia.org/MATERIALISMOFILOSOFICOesp.pdf. {frances}

Velasco, Jesús Martínez. “El Problema Mente-Cerebro: Sus Orígenes Cartesianos.” Contrastes. Revista Interdisciplinar De Filosofía 1 (1996): 201.http://www.revistas.uma.es/index.php/contrastes/article/view/1874/1818. {frances}

Kull, Kalevi. “On Semiosis, Umwelt, and Semiosphere.” Semiotica 120 (1998): 299–310. http://www.zbi.ee/~kalevi/jesphohp.htm. {frances}

Von Bertalanffy, Ludwig. “The Mind-Body Problem: A New View.” Pyshosomatic Medicine 26, no. 1 (1964): 43. http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.521.7045&rep=rep1&type=pdf. {frances}

Kuri, Ivonne, y Julio Muñoz Rubio. “El Darwinismo Neural y Los Orígenes De La 
Conciencia Humana: Una Crítica Desde La Dialéctica.” Scielo 19, no. 37 (Enero 2017). 
http://www.scielo.org.mx/scielo.php?script=sci_arttext&pid=S1665-13242017000100
170. {frances}
Srnicek, Nick y Williams, Alex. 2013. [_Accelerate Manifesto._](http://criticallegalthinking.com/2013/05/14/accelerate-manifesto-for-an-accelerationist-politics/)

Marx, Karl y Engels, Frederich. 2014. _Manifiesto del partido comunista_. Madrid: Fundación de Investigaciones Marxistas, 61.
 
Marx, Karl. 2014. “Fragment on Machines”, en _Accelerate_, editado por Robert Mackay y Armen Avanessian. Falmouth: Urbanomic Media Ltd., 63-4.

Deleuze, Gilles y Guattari, Félix. 1985. _El Anti Edipo_. _Capitalismo y esquizofrenia_. Barcelona: Paidós., 246-7
   
Lyotard, Jean-Francois. 1993. _Libidinal Economy_. Indiana: Indiana University Press., 241.
    
Noys, Benjamin. 2014. _Malign Velocities: Accelerationism and Capitalism_. Alresford: Zero Books, 34-5.
    
Laboria Cuboniks. [_Xenofeminism: A politics for alienation_](https://www.laboriacuboniks.net), 9-10.

 Por ejemplo, En 2015, Daphne fue secuestrada y violada en Veracruz por cuatro jóvenes acaudalados conocidos como _Los Porkys_. La noticia y su desarrollo están publicados en Excelsior. El artículo puede consultarse [en línea] (https://www.excelsior.com.mx/nacional/2017/03/29/1140391)

Por mencionar algunos: el caso de la joven de Atzcapotzalco que fue violada por cuatro policías el 12 de agosto de 2019; el feminicidio de Lesvy en manos de su novio el 3 de mayo de 2017; el caso de Daphne, quien fue secuestrada y violada por cuatro jóvenes acaudalados conocidos como Los Porkys en 2015.

Rita Segato, _La escritura en el cuerpo de las mujeres asesinadas en Ciudad Juárez_ (Buenos Aires: Editorial Tinta Limón, 2013), 19-21.

Este concepto no será abordado de manera especial en el dosier, por lo que puede limitarse a entiéndase por patriarcado la estructura de dominación masculina y masculinizada en la que el control de las esferas de la vida, privadas y públicas, pertenece a los hombres.

Rita Segato, _La guerra contra las mujeres_ (Madrid: Editorial Traficantes de sueños, 2016), 39.

Rita Segato, _Las nuevas formas de guerra y el cuerpo de las mujeres_ (Puebla: Editorial Pez en el árbol/Tinta Limón, 2014), 22-23.

Mariana Berlanga, _Una mirada al feminicidio_ (México: Editorial Ítaca/Universidad Autónoma de la Ciudad de México, 2018), 68-69.

Mariana Berlanga, _Una mirada al feminicidio_, 98-99.

_Lo and Behold, Revereies of the Connected World_. Directed by Werner Herzog. Magnolia Pictures, 2016.
Hito Streyerl,_ Los condenados de la pantalla_,( Buenos Aires: Caja Negra Editoriales, 2014)33. 
Varoon Bashyakarla, [_Psychometric Profiling: Persuasion by Personality in Elections, Our Data Our Selves_]. (https://ourdataourselves.tacticaltech.org/posts/psychometric-profiling/) (.frances)
Hito Streyerl, _ Los condenados de la pantalla_.(Buenos Aires: Caja Negra Editora, 2014) 33.
Jaron Lanier,_You Need Culture to Even Percieve Information Technology, You Are Not a Gadget: A Manifesto_.(New York City: Alfred A Knopf, 2010) 
Remedios Zafra,_Un cuarto propio conectado_. (Ciber)espacio y (auto)gestión del yo_. (Madrid: Fórcola Ediciones, 2010). 
_Wikipedia: About, Wikipedia_. https://en.wikipedia.org/wiki/Wikipedia:About
Sigmund Freud, El malestar en la cultura ( Madrid: Alianza Editorial, 1966) 58. 












Linda M.G. Zerilli, “A Process without a Subject: Simone de Beauvoir and Julia Kristeva on Maternity”, Sings, Vol. 18, No. 1, otoño, 1992. 132.
 
María Laura Giallorenzi, “La maternidad como proyecto individual y autónomo. El caso de las madres solas por elección”, Journal de Ciencias Sociales, Año 6, No. 11, 2018. 141-143.
  
Gayatri Chakravorty Spivak “¿Puede hablar el subalterno?” Revista Colombiana de Antropología, Vol. 39, enero-diciembre 2003. 307.
   
Stephen Morton, “Las mujeres del «tercer mundo» y el pensamiento occidental”,  La manzana de la discordia, Vol. 5, No. 1, enero-junio 2010. 117.  

Elisabeth Badinter, ¿Existe el amor maternal? Historia del amor maternal. Siglos XVII al XX, (Barcelona: Paidós/Pomaire, 1981). 309. 

Rosario Castellanos, Sobre cultura femenina, (Ciudad de México: Fondo de Cultura Económica, 2005) 215-216. 
Max Horkheimer, Crítica de la Razón Instrumental (Buenos Aires: SUR ,1973)185. 

Fernando J. Vergara Herníquez, “La modernidad revelada o la crítica ante la irrenunciabilidad al progreso en Adorno y Horkheimer”, Revista Opción 65 (septiembre-diciembre 2011): 93-115.

Stephanie Graf, “La actualidad de los “Elementos del Antisemitismo” de Adorno y Horkheimer”, Revista Signos Filosóficos 38 (julio-diciembre 2017) 118-149.

Amy Allen, “Emancipación sin utopía: Sometimiento, modernidad y las reivindicaciones normativas de la teoría crítica feminista”, Revista Signos Filosóficos 35 (enero-junio 2016):170-196.

Joseph Rouse, “The Dynamics of Power an Knowledge in Science”, The Journal of Philosophy 11 (noviemnre1991): 658-665.

L. Alcoff, “Epistemologies of Ignorance: Three types”, en Race and Epistemologies of Ignorance, Sullivan Shannon y Nancy Tuana., ed. (E.U.A: State University of New York 2007)39-49.

Francisco Daniel Tiapa-Blanco, “Colonialismo, Miradas fronterizas y denaturalización de los sustratos epistemológicos del eurocentrismo”, Revista LiminaR. Estudios Sociales y Humanísticos 1 (enero-junio 2019) ):114-126.

Fernando J. Vergara Herníquez, “La modernidad revelada o la crítica ante la irrenunciabilidad al progreso en Adorno y Horkheimer”, Revista Opción 65 (septiembre-diciembre 2011): 97.

Fernando J. Vergara Herníquez, “La modernidad revelada o la crítica ante la irrenunciabilidad al progreso en Adorno y Horkheimer”, Revista Opción 65 (septiembre-diciembre 2011): 99.

Fernando J. Vergara Herníquez, “La modernidad revelada o la crítica ante la irrenunciabilidad al progreso en Adorno y Horkheimer”, Revista Opción 65 (septiembre-diciembre 2011): 106.

Stephanie Graf, “La actualidad de los “Elementos del Antisemitismo” de Adorno y Horkheimer”, Revista Signos Filosóficos 38 (julio-diciembre 2017) 124.

Amy Allen, “Emancipación sin utopía: Sometimiento, modernidad y las reivindicaciones normativas de la teoría crítica feminista”, Revista Signos Filosóficos 35 (enero-junio 2016): 173-174. 

Amy Allen, “Emancipación sin utopía: Sometimiento, modernidad y las reivindicaciones normativas de la teoría crítica feminista”, Revista Signos Filosóficos 35 (enero-junio 2016): 179 .

Joseph Rouse, “The Dynamics of Power an Knowledge in Science”, The Journal of Philosophy 11 (noviemnre1991): 660.

L. Alcoff, “Epistemologies of Ignorance: Three types”, en Race and Epistemologies of Ignorance, Sullivan Shannon y Nancy Tuana., ed. (E.U.A: State University of New York 2007):51.

L. Alcoff, “Epistemologies of Ignorance: Three types”, en Race and Epistemologies of Ignorance, Sullivan Shannon y Nancy Tuana., ed. (E.U.A: State University of New York 2007):57.

Francisco Daniel Tiapa-Blanco, “Colonialismo, Miradas fronterizas y denaturalización de los sustratos epistemológicos del eurocentrismo”, Revista LiminaR. Estudios Sociales y Humanísticos 1 (enero-junio 2019) ):118.
